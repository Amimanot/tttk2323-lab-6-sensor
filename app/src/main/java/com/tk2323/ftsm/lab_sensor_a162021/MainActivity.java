package com.tk2323.ftsm.lab_sensor_a162021;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    LocationManager locationManager;
    LocationListener locationListener;
    TextView tv_location;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onResume() {
        super.onResume();

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 5000, 1, locationListener);
                Toast.makeText(this, "Listener Added", Toast.LENGTH_SHORT).show();
            } else
            {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION))
                {
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setTitle("Need Location Permission");
                    builder.setMessage("This app needs location permission to get the location information.");
                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                            }
                        }
                    });

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MainActivity.this, "Sorry, this function cannot run until the permission is granted", Toast.LENGTH_SHORT).show();
                        }
                    });

                    builder.show();
                } else
                {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
                }
            }
        } else
        {
            locationManager.requestLocationUpdates(locationManager.GPS_PROVIDER, 5000, 1, locationListener);
            Toast.makeText(this, "Listener Added", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(locationManager != null)
        {
            locationManager.removeUpdates(locationListener);
            Toast.makeText(this, "Listener Removed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv_location = findViewById(R.id.tv_location);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        coordinatorLayout = findViewById(R.id.layout_coordinator);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                tv_location.append("\n" + "Latitude: " + location.getLatitude() + " Longitude: " + location.getLongitude());

                Snackbar snackbar = Snackbar.make(coordinatorLayout, "Location Changed", Snackbar.LENGTH_SHORT);

                snackbar.show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }
}
